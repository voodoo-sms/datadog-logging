<?php

namespace VoodooSMS\DatadogLogging\Tests;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return ['VoodooSMS\DatadogLogging\DatadogLoggingServiceProvider'];
    }
}
