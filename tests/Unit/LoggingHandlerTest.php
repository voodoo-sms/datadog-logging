<?php

namespace VoodooSMS\DatadogLogging\Tests\Unit;

use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use TiMacDonald\Log\LogFake;
use VoodooSMS\DatadogLogging\Jobs\SendDatadogLogJob;
use VoodooSMS\DatadogLogging\Tests\TestCase;

class LoggingHandlerTest extends TestCase
{
    public function test_it_queues_a_job()
    {
        Queue::fake();

        Log::channel('datadog')->info('test');

        Queue::assertPushed(SendDatadogLogJob::class);
    }

    public function test_it_queues_a_job_when_set_to_queue()
    {
        Queue::fake();

        Log::channel('datadog')->info('test', ['ddqueue' => true]);

        Queue::assertPushed(SendDatadogLogJob::class);
    }

    public function test_it_executes_when_set_to_not_queue()
    {
        Bus::fake();

        Log::channel('datadog')->info('test', ['ddqueue' => false]);

        Bus::assertDispatched(
            SendDatadogLogJob::class
        );
    }

    public function test_it_logs_to_channel()
    {
        Queue::fake();
        Log::swap(new LogFake());

        Log::channel('datadog')->info('test');

        Log::channel('datadog')->assertLogged('info');
    }
}
