<?php

namespace VoodooSMS\DatadogLogging\Tests\Unit;

use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use TiMacDonald\Log\LogFake;
use VoodooSMS\DatadogLogging\Jobs\SendDatadogLogJob;
use VoodooSMS\DatadogLogging\Tests\TestCase;

class HelperFunctionTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Queue::fake();
    }

    public function test_it_queues_a_job()
    {
        datadog('test');

        Queue::assertPushed(SendDatadogLogJob::class);
    }

    public function test_it_executes_when_set_to_not_queue()
    {
        Bus::fake();

        datadog('test', [], 'info', false);

        Bus::assertDispatched(
            SendDatadogLogJob::class
        );
    }

    public function test_it_logs_to_channel()
    {
        Log::swap(new LogFake());

        datadog('test');

        Log::channel('datadog')->assertLogged('info');
    }
}
