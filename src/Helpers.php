<?php

use Illuminate\Support\Facades\Log;

if (!function_exists('datadog')) {
    /**
     * Simple helper command to log to datadog.
     *
     * @param string $message   The log message
     * @param array $context    The log context
     * @param string $level The log level, e.g. info
     * @param bool $queued  Whether the log should be queued or sent synchronously
     * @return void
     */
    function datadog(string $message, array $context = [], string $level = 'info', bool $queued = true)
    {
        if ($queued !== true) {
            $context['ddqueue'] = false;
        }

        Log::channel('datadog')->$level($message, $context);
    }
}
