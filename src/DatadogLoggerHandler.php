<?php

namespace VoodooSMS\DatadogLogging;

use Monolog\Handler\AbstractProcessingHandler;

class DatadogLoggerHandler extends AbstractProcessingHandler
{
    public function write(array $record): void
    {
        DatadogLogger::log(
            $record['message'],
            $record['context']['event'] ?? 'generic event',
            $record['level_name'],
            $record['context'],
            (bool) ($record['context']['ddqueue'] ?? true)
        )->send();
    }
}
