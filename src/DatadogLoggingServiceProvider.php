<?php

namespace VoodooSMS\DatadogLogging;

use Graze\TelnetClient\TelnetClient;
use Illuminate\Support\ServiceProvider;

class DatadogLoggingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        require __DIR__ . '/Helpers.php';

        $this->app->bind('datadog-telnet', function () {
            return TelnetClient::factory();
        });

        $this->mergeConfigFrom(__DIR__ . '/config/datadog.php', 'datadog');
        config(['logging.channels.datadog' => require(__DIR__ . '/config/logging.php')]);
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/config/datadog.php' => config_path('datadog.php'),
            ], 'config');
        }
    }
}
