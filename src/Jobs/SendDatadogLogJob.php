<?php

namespace VoodooSMS\DatadogLogging\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

class SendDatadogLogJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private $level;
    private $event;
    private $message;
    private $other;

    private $url;
    private $port;
    private $apiKey;
    private $service;
    private $source;
    private $tags;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        string $level,
        string $event,
        string $message,
        array $other
    ) {
        $this->level = $level;
        $this->event = $event;
        $this->message = $message;
        $this->other = $other;

        $this->url = config('datadog.logging.url');
        $this->port = config('datadog.logging.port');
        $this->apiKey = config('datadog.api_key');
        $this->service = config('datadog.logging.service');
        $this->source = config('datadog.logging.source');
        $this->tags = config('datadog.logging.tags');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $log = json_encode(
            array_merge(
                [
                    'message' => $this->level . ': ' . $this->message,
                    'level' => $this->level,
                    'event' => $this->event,
                    'ddtags' => $this->tags,
                    'ddsource' => $this->source,
                    'service' => $this->service,
                ],
                $this->other
            )
        );

        $client = app()->make('datadog-telnet');

        $client->connect($this->url . ':' . $this->port);
        $client->setReadTimeout(0.01);

        try {
            $client->execute($this->apiKey . ' ' . $log);
        } catch (Throwable $t) {
            // do nothing, always returns socket error because datadog
            // doesn't respond in the telnet connection
        }
    }
}
