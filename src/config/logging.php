<?php

return [

    'driver' => 'monolog',
    'level' => 'debug',
    'handler' => VoodooSMS\DatadogLogging\DatadogLoggerHandler::class,

];
