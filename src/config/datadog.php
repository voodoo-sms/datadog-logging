<?php

return [

    'api_key' => env('DATADOG_APIKEY', ''),
    'logging' => [
        'url' => env('DATADOG_LOGGING_URL', ''),
        'port' => (int) env('DATADOG_LOGGING_PORT', ''),
        'tags' => env('DATADOG_LOGGING_TAGS', ''),
        'service' => env('DATADOG_LOGGING_SERVICE', ''),
        'source' => env('DATADOG_LOGGING_SOURCE', ''),
    ],

];
