<?php

namespace VoodooSMS\DatadogLogging;

use VoodooSMS\DatadogLogging\Jobs\SendDatadogLogJob;

class DatadogLogger
{
    protected $message;
    protected $level;
    protected $event;
    protected $other;
    protected $queued;

    /**
     * Quickly log a message.
     * These are chained so the log handler can call it statically.
     *
     * @param string $message
     * @param string $level
     * @return self
     */
    public static function log(string $message, string $event, string $level, array $other, bool $queued = true): self
    {
        return (new DatadogLogger())
            ->setMessage($message)
            ->setEvent($event)
            ->setLevel($level)
            ->setOther($other)
            ->setQueued($queued);
    }

    /**
     * Dispatch a log message.
     *
     * @return bool
     */
    public function send(): bool
    {
        if (isset($this->other['ddqueue'])) {
            unset($this->other['ddqueue']);
        }

        $job = new SendDatadogLogJob(
            (string) $this->level,
            $this->event,
            $this->message,
            $this->other,
        );

        if ($this->queued) {
            dispatch($job)->onQueue('datadog');
        } else {
            dispatch_now($job);
        }

        return true;
    }

    /**
     * Sets the log message.
     *
     * @param  string $message
     * @return self
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Sets the log queue type.
     *
     * @param  bool $queued
     * @return self
     */
    public function setQueued(bool $queued): self
    {
        $this->queued = $queued;

        return $this;
    }

    /**
     * Sets the log level.
     *
     * @param  string $level
     * @return self
     */
    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Sets the event name.
     *
     * @param  string $event
     * @return self
     */
    public function setEvent(string $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Set the interests for Push notifications.
     *
     * @param  array $user
     * @return self
     */
    public function setOther(array $other): self
    {
        $this->other = $other;

        return $this;
    }
}
