# Datadog Logging

## Installation

```shell
composer require voodoosms/datadog-logging
```

## Usage

You need to set up some env variables for this to work:

```
DATADOG_APIKEY=
DATADOG_LOGGING_URL=tcp-intake.logs.datadoghq.eu
DATADOG_LOGGING_PORT=1883
DATADOG_LOGGING_SOURCE=
DATADOG_LOGGING_SERVICE=
DATADOG_LOGGING_TAGS=
```

Then you can use it using Laravel's `Log` facade:

```php
Log::channel('datadog')->info('message here')
```

You should set the name of the event when logging with this though:

```php
Log::channel('datadog')->info('message here', ['event' => 'some event'])
```

Anything passed into the array will also be included as a field in the datadog log:

```php
Log::channel('datadog')->info('message here', ['event' => 'some event', 'user' => $user])
```

Logs are sent to datadog using telnet, so the `DatadogLoggerHandler` dispatches a `SendDatadogLogJob`, so you will need a queue worker running for these logs to get to datadog, using the queue `datadog`:

```php
php artisan queue:work --queue=datadog
```

Alternatively, you can send these logs synchronously by adding `'ddqueue' => false` to the log context:

```php
Log::channel('datadog')->info('message here', ['ddqueue' => false])
```

You can also use the `datadog` helper function to quickly send a log:

```php
datadog('test', ['user' => $user]);
```